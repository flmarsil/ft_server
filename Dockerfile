FROM debian:buster 

#Update && upgrade distro + download utils tools
RUN     apt-get update  -y && apt-get upgrade -y
RUN     apt-get install -y vim
RUN     apt-get install -y curl
RUN     apt-get install -y wget

#Nginx
RUN     apt-get install -y nginx
RUN 	rm /etc/nginx/sites-available/default
RUN     rm /etc/nginx/sites-enabled/default
COPY	srcs/conf/monsite.conf /etc/nginx/sites-available/
RUN 	ln -s /etc/nginx/sites-available/monsite.conf /etc/nginx/sites-enabled/monsite.conf

#Home page installation
RUN 	rm -rf /var/www/*
RUN 	mkdir -p /var/www/MONSITE/html/images
COPY    srcs/html/index.html /var/www/MONSITE/html
COPY    srcs/html/logo_wordpress.jpg /var/www/MONSITE/html/images
COPY    srcs/html/logo_phpmyadmin.jpg /var/www/MONSITE/html/images

#PHP
RUN 	apt-get install -y php-fpm
RUN     apt-get install -y php-mysql
RUN     apt-get install -y php-mbstring
RUN     rm /etc/php/7.3/fpm/pool.d/www.conf
COPY    srcs/conf/www.conf /etc/php/7.3/fpm/pool.d/

#MySQL (mariadb)
RUN 	apt-get install -y mariadb-server
RUN 	apt-get install -y mariadb-client

WORKDIR /var/www/MONSITE/html

#phpMyAdmin
RUN     wget "https://www.phpmyadmin.net/downloads/phpMyAdmin-latest-all-languages.tar.gz"
RUN     tar -xzvf phpMyAdmin-latest-all-languages.tar.gz
RUN     rm phpMyAdmin-latest-all-languages.tar.gz
RUN 	mv phpMyAdmin-5.0.2-all-languages phpmyadmin
COPY    srcs/conf/phpmyadmin.conf /etc/nginx/sites-available/
RUN     ln -s /etc/nginx/sites-available/phpmyadmin.conf /etc/nginx/sites-enabled/phpmyadmin.conf

#Wordpress
RUN     wget "http://wordpress.org/latest.tar.gz"
RUN     tar -xzvf latest.tar.gz
RUN     rm latest.tar.gz
RUN 	rm wordpress/wp-config-sample.php
COPY	srcs/conf/wp-config.php /var/www/MONSITE/html/wordpress

#Create the MySQL WordPress user
RUN		service mysql start && \
		echo "CREATE DATABASE wordpress;" | mysql -u root && \
		echo "CREATE USER wordpress@localhost;" | mysql -u root && \
		echo "SET PASSWORD FOR wordpress@localhost = PASSWORD('[password]');" | mysql -u root && \
		echo "GRANT ALL PRIVILEGES ON wordpress.* TO wordpress@localhost IDENTIFIED BY '[password]';" | mysql -u root && \
		echo "FLUSH PRIVILEGES;"| mysql -u root && exit

#Update permissions for Nginx user
RUN     chown www-data:www-data * -R 
RUN     usermod -a -G www-data www-data

#SSL auto-certification 
RUN     apt-get install -y libnss3-tools
RUN 	mkdir ~/.mkcert
WORKDIR /root/.mkcert
RUN 	wget "https://github.com/FiloSottile/mkcert/releases/download/v1.1.2/mkcert-v1.1.2-linux-amd64"
RUN 	mv mkcert-v1.1.2-linux-amd64 mkcert
RUN 	chmod +x mkcert
RUN 	./mkcert -install
RUN 	./mkcert localhost
RUN 	cp /root/.mkcert/* /etc/nginx/

#Clean-up
RUN     apt-get clean 
RUN     apt-get autoclean
RUN     apt-get autoremove --purge
RUN     rm -Rf ~./local/share/Trash/*
RUN     rm -Rf /root/.local/share/Trash/*
RUN     rm -Rf /var/cache/apt/archives/*

EXPOSE  80 443

WORKDIR /var/www/
COPY    srcs/conf/start_services.sh /var/www/

CMD 	["sh /var/www/start_services.sh"]
